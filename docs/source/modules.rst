.. index:: subpackages and modules

.. _skopt:

=====================================
Subpackage/module Reference
=====================================

SKOPT currently includes the following sub-packages:

.. list-table::

    * - :ref:`core <skopt.core>` 
      - the core modules realising the optimisation framework

    * - :ref:`dftbutils <skopt.dftbutils>` 
      - the modules related to a DFTB model

.. toctree::

    skopt.core
    skopt.dftbutils
